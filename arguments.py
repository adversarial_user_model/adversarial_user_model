#!/usr/bin/python
# encoding: utf-8


#==================================
#@file name: arguments
#@author: Lixin Zou
#@contact: zoulixin15@gmail.com
#@time:18/10/23,下午10:42
#==================================

import numpy as np
import ipdb
import argparse
import utils
import os
import tensorflow as tf
# adding breakpoint with ipdb.set_trace()

def str2bool(v):
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')

@utils.Pipe
def parse_arguments(*args):
    parser = argparse.ArgumentParser(description='training config')
    parser.add_argument('-log_path',dest='log_path',type = str,default="./log/",help='log_path')
    parser.add_argument('-model',dest='model',type = str,default="model",help='model name')
    parser.add_argument('-GPU_index',dest='GPU_index',type=str,default="0",help="the deploying GPU id")
    parser.add_argument('-env', dest='env', type=str, default='CartPole-v0', help='the default training environment.')
    parser.add_argument('-save_path', dest='save_path', type=str, default='./saved_model',help='the training save path.')
    parser.add_argument('-task', dest='task', type=str, default='train',help='task.')
    parser.add_argument('-batch', dest='batch', type=int, default=128,help='batch size')
    parser.add_argument('-discount_factor', dest='discount_factor', type=float, default=0.9,help='discount_factor')
    parser.add_argument('-saved_model_path', dest='saved_model_path', type=str, default="./saved_model/",help='discount_factor')
    parser.add_argument('-memory', dest='memory', type=int, default=50000,help='memory size')
    args = parser.parse_args()
    args.GPU_OPTION = tf.GPUOptions(allow_growth=True)
    return args

@utils.Pipe
def initialize(config,*args):
    if not os.path.isdir(config.log_path):
        os.mkdir(config.log_path)
    utils.log.set_log_path(os.path.join(config.log_path,config.model)+"_"+str(config.task)+"_"+str(utils.get_now_time())+".log")
    utils.log.info('saving log file in '+utils.log().log_path)
    utils.log.structure_info("config for experiments",list(vars(config).items()))
    return config