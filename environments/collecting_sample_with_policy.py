#!/usr/bin/python
# encoding: utf-8


#==================================
#@file name: collecting_sample_with_policy
#@author: Lixin Zou
#@contact: zoulixin15@gmail.com
#@time:18/10/19,上午4:07
#=================================utils

import numpy as np
import ipdb
from tqdm import trange
import utils
import argparse

class collecting_sample_with_policy(object):
    def __init__(self,policy,environments,sample_number = 10000,save_path="./sample.pkl"):
        self.histories = []
        self.policy = policy
        self.env = environments
        self.sample_number = sample_number
        self.save_path = save_path
        self.collecting_samples()

    def run_game(self):
        done = True
        next_state = None
        while len(self.histories)<=self.sample_number:
            if done:
                state = self.env.reset()
            else:
                state = next_state
            action = self.policy(state)
            next_state,reward,done,info = self.env.step(action)
            yield state,action,reward,next_state

    def collecting_samples(self):
        for sars in self.run_game():
            self.histories.append(sars)
        utils.saveobj2files(self.histories,self.save_path)


