#!/usr/bin/python
# encoding: utf-8


#==================================
#@file name: CartPole
#@author: Lixin Zou
#@contact: zoulixin15@gmail.com
#@time:18/10/22,上午9:31
#==================================

import numpy as np
import ipdb
from .base import *
import tensorflow as tf

class CartPole_Q_value(basic_model):
    def _create_placeholders(self):
        self.state = tf.placeholder(tf.float32,(None,4))
        self.action = tf.placeholder(tf.float32,(None,2))
        self.target = tf.placeholder(tf.float32,(None,))

    def _create_inference(self):
        state_action = tf.concat([self.state,self.action], 1)
        sa = tf.layers.dense(state_action,10,bias_initializer=tf.zeros_initializer,
                                  trainable=self.trainable,activation=tf.sigmoid)
        self.q = tf.reshape(tf.layers.dense(sa,1,
                                  bias_initializer=tf.zeros_initializer,
                                  trainable=self.trainable), (-1,))

    def _create_optimizer(self):
        self.loss = tf.losses.mean_squared_error(self.target,self.q)
        self.optimizer = tf.train.AdamOptimizer(learning_rate=0.01).minimize(self.loss,global_step=self.global_step)

    def _update_placehoders(self):
        self.placeholders["all"] = {"state":self.state,
                                    "action":self.action,
                                    "target":self.target}
        predicts = ["state","action"]
        self.placeholders["predict"]={item:self.placeholders["all"][item] for item in predicts}
        self.placeholders["optimizer"] = self.placeholders["all"]

    def optimize_model(self,sess,data):
        feed_dicts = self._get_feed_dict("optimizer",data)
        return sess.run([self.loss,self.optimizer],feed_dicts)[0]

    def predict(self,sess,data):
        feed_dicts = self._get_feed_dict("predict", data)
        return sess.run(self.q, feed_dicts)