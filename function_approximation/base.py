#!/usr/bin/python
# encoding: utf-8


#==================================
#@file name: base
#@author: Lixin Zou
#@contact: zoulixin15@gmail.com
#@time:18/10/19,上午4:06
#==================================
import numpy as np
import ipdb
import tensorflow as tf
from utils.zlog import log

class basic_model(object):
    GRAPHS = {}
    SESS = {}
    SAVER = {}

    def load_initial_matrix(self):
        initial_matrix = np.random.uniform(-0.001,0.001,(self.config.ITEM_NUMBER,self.config.RNN_HIDDEN))
        with open(self.config.SKU_EMBEDDING,"r") as f:
            for line in f.readlines():
                line = line.strip("\n").split("\t")
                initial_matrix[int(line[0])-1,:] = np.asarray(line[1:])
        return initial_matrix

    @classmethod
    def create_model(cls, config, variable_scope = "target", trainable = True, graph_name="DEFAULT"):
        log.info("CREATE MODEL", config.model, "GRAPH", graph_name, "VARIABLE SCOPE", variable_scope)
        if not graph_name in cls.GRAPHS:
            log.info("Adding a new tensorflow graph:",graph_name)
            cls.GRAPHS[graph_name] = tf.Graph()
        with cls.GRAPHS[graph_name].as_default():
            model = cls(config, variable_scope=variable_scope, trainable=trainable)
            if not graph_name in cls.SESS:
                cls.SESS[graph_name] = tf.Session(config = tf.ConfigProto(gpu_options=config.GPU_OPTION))
                cls.SAVER[graph_name] = tf.train.Saver(max_to_keep=20000)
            cls.SESS[graph_name].run(model.init)
        return {"graph": cls.GRAPHS[graph_name],
               "sess": cls.SESS[graph_name],
               "saver": cls.SAVER[graph_name],
               "model": model}

    def _update_placehoders(self):
        self.placeholders = {"none":{}}
        raise NotImplemented

    def _get_feed_dict(self,task,data_dicts):
        place_holders = self.placeholders[task]
        res = {}
        for key, value in place_holders.items():
            res[value] = data_dicts[key]
        return res

    def __init__(self, config, variable_scope = "target", trainable = True):
        self.config = config
        self.variable_scope = variable_scope
        self.trainable = trainable
        self.placeholders = {}
        self._build_model()

    def _build_model(self):
        with tf.variable_scope(self.variable_scope):
            self._create_placeholders()
            self._create_global_step()
            self._update_placehoders()
            self._create_inference()
            if self.trainable:
                self._create_optimizer()
            self._create_intializer()

    def _create_global_step(self):
        self.global_step = tf.Variable(0, dtype=tf.int32, trainable=False, name='global_step')

    def _create_intializer(self):
        with tf.name_scope("initlializer"):
            self.init = tf.global_variables_initializer()

    def _create_placeholders(self):
        raise NotImplementedError

    def _create_inference(self):
        raise NotImplementedError

    def _create_optimizer(self):
        raise NotImplementedError

    def optimize_model(self,*args):
        raise NotImplementedError

    def build_cell(self,size,rnn_type,initializer,hidden):
        if rnn_type == "lstm":
            cell = tf.nn.rnn_cell.LSTMCell(size, state_is_tuple=True,
                                           initializer=initializer)
        elif rnn_type == "gru":
            cell = tf.nn.rnn_cell.GRUCell(hidden,
                                          kernel_initializer=initializer,
                                          bias_initializer=tf.zeros_initializer)
        else:
            cell = tf.nn.rnn_cell.BasicRNNCell(hidden)
        return cell