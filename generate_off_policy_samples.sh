#!/usr/bin/env bash
run (){
    ~/pyenv3/bin/python ./generating_off_policy_samples.py -policy random -env $1 \
    -sample_number $2 -save_path $3;
}
run CartPole-v0 300000 ./data/CartPole_off_policy.pkl