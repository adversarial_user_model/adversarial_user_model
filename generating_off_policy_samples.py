#!/usr/bin/python
# encoding: utf-8


#==================================
#@file name: generating_off_policy_samples
#@author: Lixin Zou
#@contact: zoulixin15@gmail.com
#@time:18/10/19,上午9:56
#==================================

import numpy as np
import ipdb
import utils
import argparse
import gym
from environments import collecting_sample_with_policy

def parse_arguments(*args):
    parser = argparse.ArgumentParser(description='sampling training data')
    parser.add_argument('-policy',dest='policy',type = str,default='random',help='what kind of policy for generating the training data.')
    parser.add_argument('-env',dest='env',type=str,default='CartPole-v0',help='the default training environment.')
    parser.add_argument('-sample_number',dest="sample_number",type=int,default=100000,help='the off-policy training sample number.')
    parser.add_argument('-save_path',dest='save_path',type=str,default='./data/test.pkl',help='the training save path.')
    args = parser.parse_args()
    return args

if __name__ == '__main__':
    config = parse_arguments()
    env = gym.make(config.env)
    policy = None
    if config.policy == "random":
        policy = lambda x:env.action_space.sample()
    collecting_sample_with_policy(policy,env,config.sample_number,save_path=config.save_path)