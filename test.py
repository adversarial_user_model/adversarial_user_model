#!/usr/bin/python
# encoding: utf-8


#==================================
#@file name: test
#@author: Lixin Zou
#@contact: zoulixin15@gmail.com
#@time:18/10/24,上午2:49
#==================================

import numpy as np
import ipdb


import gym
env = gym.make('CartPole-v0')
env.reset()
for i in range(1000):
    env.render()
    env.step(i%2) # take a random action