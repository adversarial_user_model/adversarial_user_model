#!/usr/bin/python
# encoding: utf-8


#==================================
#@file name: qlearning
#@author: Lixin Zou
#@contact: zoulixin15@gmail.com
#@time:18/10/19,上午11:42
#==================================

import numpy as np
import ipdb
import gym
import function_approximation
from utils.zlog import log
import utils
import tensorflow as tf
import os
class online_q_learning(object):
    def save_agent(self,agent,save_time):
        utils.log.info("SAVE AGENT",save_time)
        utils.save_model(agent["saver"],
                        utils.create_saved_path(save_time,self.config.saved_model_path,
                                               self.config.model),
                        agent["sess"],
                        agent["model"].global_step)

    def load_model(self,agent,save_time):
        path = os.path.join(self.config.saved_model_path,self.config.model+"_"+str(save_time))
        agent['saver'].restore(agent['sess'],tf.train.latest_checkpoint(path))
        pass

    def __init__(self,config):
        self.config = config
        self.init_train()
        self.memory = []
    def init_train(self):
        self.env = gym.make(self.config.env)
        self.model = function_approximation.models[self.config.model].create_model(self.config,variable_scope="training",trainable=True,graph_name="default")

    def run(self):
        done = True
        self.update_number = 0
        self.best_performance = 0.0
        while True:
            state = self.env.reset()
            for _ in range(1000):
                action = self.choose_action(state)
                next_state, reward, done, info = self.env.step(action)
                if done:
                    reward = -2
                self.memory.append((state,action,reward,next_state))
                state = next_state
                if len(self.memory)>self.config.memory+self.config.batch:
                    self.update_model()

    def choose_action(self,state,greedy=0.9):
        if np.random.random()>greedy:
            action = self.env.action_space.sample()
        else:
            data ={'state':[state,state],'action':[[1,0],[0,1]]}
            action = np.argmax(self.model["model"].predict(self.model["sess"],data))
        return action

    def next_q_value(self,state):
        action_zero = [[1,0]]*len(state)
        action_one = [[0,1]]*len(state)
        q_zero = self.model["model"].predict(self.model["sess"],{"state":state,"action":action_zero})
        q_one = self.model["model"].predict(self.model["sess"],{"state":state,"action":action_one})
        return np.max(np.concatenate((q_zero.reshape((-1,1)),q_one.reshape((-1,1))),axis=1),axis=1)


    def update_model(self):
        self.memory = self.memory[self.config.batch:]
        batch = np.asarray(self.memory)[np.random.choice(len(self.memory),(self.config.batch,))]
        state = [item[0] for item in batch]
        action = [[[1,0],[0,1]][item[1]] for item in batch]
        r = [item[2] for item in batch]
        next_state = [item[3] for item in batch]
        target = np.asarray(r)+self.config.discount_factor*self.next_q_value(next_state)
        loss = self.model["model"].optimize_model(self.model["sess"],{"state":state,"action":action,"target":target})
        self.update_number+=1
        if self.update_number%50 == 0:
            mean_rewards = np.mean([item[2] for item in self.memory])
            # if mean_rewards > self.best_performance:
            #     self.best_performance = mean_rewards
            self.save_agent(self.model,self.update_number)
            log.info("loss",loss,"reward",mean_rewards)

    def evaluate(self):
        processing = []
        while True:
            saved_model = os.listdir(os.path.abspath(self.config.saved_model_path))
            saved_model = [int(item.split("_")[-1]) for item in saved_model if item.__contains__(self.config.model)]
            saved_model.sort(reverse=False)
            for num in saved_model:
                if num in processing:
                    continue
                print("#"*30+"evluating:")
                r= self.evaluate_model(num)
                processing.append(num)
                log.info("reward",r)

    def evaluate_model(self,num):
        self.load_model(self.model, num)
        state = self.env.reset()
        rewards_all = 0
        for _ in range(1000):
            # self.env.render()
            action = self.choose_action(state,1.0)
            state, reward, done, info = self.env.step(action)
            rewards_all+=reward
        return rewards_all




