#!/usr/bin/env bash

run (){
    ~/pyenv3/bin/python ./training_model.py -log_path ./log/ -model \
    $1 -GPU_index $2 -env $3 -save_path ./saved_model/ -task $4 -batch 128 \
    -memory 10000 -discount_factor 0.9;
}
#run CartPole-v0-Q 2 CartPole-v0 CartPole-v0-online-Q-learning-train
run CartPole-v0-Q 2 CartPole-v0 CartPole-v0-online-Q-learning-evaluate