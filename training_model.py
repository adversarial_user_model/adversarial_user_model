#!/usr/bin/python
# encoding: utf-8


#==================================
#@file name: training_model
#@author: Lixin Zou
#@contact: zoulixin15@gmail.com
#@time:18/10/23,下午11:32
#==================================

import numpy as np
import ipdb
from arguments import parse_arguments,initialize
import utils
import os
from train import online_q_learning
import time

if __name__ == '__main__':
    import time
    np.random.seed(int(time.time()))
    config = []|parse_arguments|initialize
    os.environ["CUDA_VISIBLE_DEVICES"] = str(config.GPU_index)
    if str(config.task).__contains__("train"):
        online_q_learning(config).run()
    if str(config.task).__contains__("evaluate"):
        online_q_learning(config).evaluate()
