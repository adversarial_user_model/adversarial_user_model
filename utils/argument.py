#!/usr/bin/python
# encoding: utf-8


#==================================
#@file name: argments
#@author: Lixin Zou
#@contact: zoulixin15@gmail.com
#@time:18/10/19,上午4:32
#==================================

import numpy as np
import ipdb
import argparse

def str2bool(v):
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')